/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost
 Source Database       : testapi

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : utf-8

 Date: 12/22/2017 10:05:20 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `api_error_types`
-- ----------------------------
DROP TABLE IF EXISTS `api_error_types`;
CREATE TABLE `api_error_types` (
  `et_id` int(3) NOT NULL AUTO_INCREMENT,
  `et_error_type` varchar(200) DEFAULT NULL,
  `et_description` text,
  PRIMARY KEY (`et_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `api_error_types`
-- ----------------------------
BEGIN;
INSERT INTO `api_error_types` VALUES ('1', 'validation', 'form validation'), ('2', 'data not found', 'query returned empty dataset'), ('3', 'checkout error', 'error from the Authorize.net API'), ('4', 'duplicate data', 'the posted value already exisits'), ('5', 'login', 'email and password do not match');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
