/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost
 Source Database       : testapi

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : utf-8

 Date: 12/22/2017 10:05:12 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `api_error_data`
-- ----------------------------
DROP TABLE IF EXISTS `api_error_data`;
CREATE TABLE `api_error_data` (
  `er_id` int(5) NOT NULL AUTO_INCREMENT,
  `er_error_code` varchar(100) NOT NULL,
  `er_status_code` int(3) DEFAULT NULL,
  `er_error_type_id` int(3) NOT NULL,
  `er_error_message` varchar(255) DEFAULT NULL,
  `er_note` text,
  PRIMARY KEY (`er_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `api_error_data`
-- ----------------------------
BEGIN;
INSERT INTO `api_error_data` VALUES ('1', 'data_100', '200', '2', 'No records where found that match your request.', 'No data was found that matched the request by the client.'), ('2', 'login_110', '200', '5', 'Password does not match', 'At login, the email was found, but the password did not match.'), ('3', 'login_115', '200', '5', 'Email address was not found.', 'The email address used was not found in the database.'), ('4', 'val_100', '202', '1', 'Value can contain only alphanumeric characters', 'Value can contain only alphanumeric characters'), ('5', 'val_101', '202', '1', 'Email address is not correct format', 'Improperly formatted email address');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
